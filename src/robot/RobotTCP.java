/**
 *
 * @author Artem Gribanov gribaart@fel.cvut.cz
 *
 */
package robot;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import robot.Headquarters.Factory;
import static robot.HeadquartesAbstract.NULL;
import static robot.HeadquartesAbstract.SYN;

/**
 * @author Artem Gribanov gribaart@fel.cvut.cz
 */
public class RobotTCP {

    public static void main(String[] args) throws IOException {
        HeadquartersInterface headquarters;
        headquarters = new Headquarters("109.239.210.2");
        headquarters.download();
//        switch (args.length) {
//            case 1:
//                headquarters = new Headquarters(args[0]);
//                headquarters.download();
//                break;
//            case 2:
//                headquarters = new Headquarters(args[0], args[1]);
//                headquarters.upload();
//                break;
//            default:
//                System.out.println("download photo: java.RobotUdp <hostname>");
//                System.out.println("upload firmware: java.RobotUdp <hostname> <filename>");
//                System.exit(1);
//
//        }

    }
}

/**
 * @author Artem Gribanov gribaart@fel.cvut.cz
 */
interface HeadquartersInterface {

    public void download();

    public void upload();

}

/**
 * @author Artem Gribanov gribaart@fel.cvut.cz
 */
abstract class HeadquartesAbstract implements HeadquartersInterface {

    //constants
    int port = 4000;
    static final int TIMEOUT = 100;
    static final byte SYN = 0x4;
    static final byte FIN = 0x2;
    static final byte RST = 0x1;
    static final byte DATA = 0x0;
    static final byte BAD_PACKET = Byte.MIN_VALUE;
    static final short NULL = 0;

    //variables
    final InetAddress host;
    final DatagramSocket socket;
    final long startTime;
    final String filename;
    int connectionId;
    Factory factory;

    public HeadquartesAbstract(String hostname, String filename) throws UnknownHostException, SocketException {

        this.host = InetAddress.getByName(hostname);
        this.socket = new DatagramSocket();
        this.socket.setSoTimeout(TIMEOUT);
        this.filename = filename;
        this.startTime = System.currentTimeMillis();
        this.connectionId = 0;
    }

}

/**
 * @author Artem Gribanov gribaart@fel.cvut.cz
 */
class Headquarters extends HeadquartesAbstract {

    private static Logger log = Logger.getLogger(Headquarters.class.getName());

    public Headquarters(String hostname) throws IOException {
        super(hostname, null);
        super.factory = new Factory();
    }

    public Headquarters(String hostname, String filename) throws IOException {
        super(hostname, filename);
        super.factory = new Factory();

    }

    /**
     * @author Artem Gribanov gribaart@fel.cvut.cz
     */
    @Override
    public void download() {
        try {
            factory.getConnector().download();

            if (factory.getReciver("foto.png").receivePacket()) {
                System.out.println("download was complete");
            } else {
                System.out.println("BIG ERROR - foto");
            }

        } catch (IOException ex) {
            log.info("Chyba - connection/download");

        }

    }

    /**
     * @author Artem Gribanov gribaart@fel.cvut.cz
     */
    @Override
    public void upload() {
        try {
            factory.getConnector().upload();

//            if (factory.getReciver("foto.png").receivePacket()) {
//                System.out.println("upload was complete");
//            } else {
//                System.out.println("BIG ERROR - frimware");
//            }
        } catch (IOException ex) {
            log.info("Chyba - connection/upload");

        }
    }

    /**
     * @author Artem Gribanov gribaart@fel.cvut.cz
     */
    private class Packet {

        DatagramPacket packet;
        int connectionId;
        short seq;
        short ack;
        byte flag;
        byte[] data;

        public Packet() {
            this.packet = factory.getDatagramPacket();
        }

        public void sendPacket(int connectionId, short seq, short ack, byte flag) throws IOException {
            sendPacket(connectionId, seq, ack, flag, null);
        }

        public void sendPacket(int connectionId, short seq, short ack, byte flag, byte data) throws IOException {
            byte[] masivDat = {data};
            sendPacket(connectionId, seq, ack, flag, masivDat);
        }

        public void sendPacket(int connectionId, short seq, short ack, byte flag, byte[] data) throws IOException {
            this.connectionId = connectionId;
            this.seq = seq;
            this.ack = ack;
            this.flag = flag;
            this.data = data;
            send();
        }

        /**
         * @author Artem Gribanov gribaart@fel.cvut.cz
         */
        private void send() throws IOException {
            byte[] packetSend = packetConstructor();
            socket.send(new DatagramPacket(packetSend, packetSend.length, host, port));
            String dataText = "";
            if (data == null) {
                data = new byte[0];
            }
            if (data.length > 0) {
                StringBuilder sb = new StringBuilder(" : ");
                for (int j = 0; j < data.length; j++) {
                    sb.append(String.format("%02X ", data[j]));
                    if (j >= 7) {
                        break;
                    }
                }
                if (data.length > 7) {
                    sb.append("...");
                }
                dataText = sb.toString();
            }
            System.out.printf("%d %08X SEND seq=%d ack=%d flags=%02d data(%d)%s%n", System.currentTimeMillis() - startTime, connectionId, seq, ack, flag, data.length, dataText);
        }

        /**
         * @author Artem Gribanov gribaart@fel.cvut.cz
         */
        private byte[] packetConstructor() {
            byte[] p = factory.getBytePacket(data);
            int i = 0;
            byte[] connIdBM = IntegerToByte(connectionId, 4);
            byte[] seqBM = IntegerToByte(seq, 2);
            byte[] ackBM = IntegerToByte(ack, 2);

            for (byte b : connIdBM) {
                p[i++] = b;
            }

            for (byte b : seqBM) {
                p[i++] = b;
            }

            for (byte b : ackBM) {
                p[i++] = b;
            }

            p[i++] = (byte) flag;

            if (null != data) {
                for (byte b : data) {
                    p[i++] = b;
                }
            }
            return p;
        }

        /**
         * @author Artem Gribanov gribaart@fel.cvut.cz
         */
        private byte[] IntegerToByte(int integer, int length) {

            byte[] array = new byte[length];

            for (int i = length - 1; i >= 0; i--) {
                array[i] = (byte) (integer & 0xff);
                integer >>= 8;
            }

            return array;
        }

        /**
         * @author Artem Gribanov gribaart@fel.cvut.cz
         */
        private void read() {
            ByteBuffer buffer = ByteBuffer.wrap(packet.getData());
            connectionId = buffer.getInt();
            seq = buffer.getShort();
            ack = buffer.getShort();
            flag = buffer.get();
            data = new byte[packet.getLength() - 9];

            for (int i = 0; i < packet.getLength() - 9; i++) {
                data[i] = buffer.get();
            }

        }

        /**
         * @author Artem Gribanov gribaart@fel.cvut.cz
         */
        private int checkSeq(int seqNum) {

            while (seqNum % 255 != 0) {
                seqNum += 65536;
            }

            return seqNum;
        }

        public void setConnectionId(int connectionId) {
            this.connectionId = connectionId;
        }

        public void setSeq(short seq) {
            this.seq = seq;
        }

        public void setAck(short ack) {
            this.ack = ack;
        }

        public void setFlag(byte flag) {
            this.flag = flag;
        }

        public void setData(byte[] data) {
            this.data = data;
        }

    }

    private class ConnectorHandler {

        public boolean download() throws IOException {
            return connect((byte) 0x1);

        }

        public boolean upload() throws IOException {
            return connect((byte) 0x2);
        }

        private boolean connect(byte mode) throws IOException {
            Packet packet = factory.getPacket();
            int ctr = 1;

            do {
                ctr++;
                long localTimeOute = System.currentTimeMillis();
                packet.sendPacket(NULL, NULL, NULL, SYN, mode);
                while (System.currentTimeMillis() - localTimeOute < TIMEOUT) {
                    try {
                        socket.receive(packet.packet);
                    } catch (IOException ex) {
                        break;
                    }
                    packet.read();
                    System.out.println(packet);
                    if (packet.data[0] == mode && packet.flag == SYN) {
                        connectionId = packet.connectionId;
                        break;
                    }
                }
            } while (0 == packet.connectionId && ctr < 21);
            return 0 != packet.connectionId;
        }
    }

    private class Reciver {

        //reciver
        private final FileOutputStream fileOutput;
        private final LinkedList<byte[]> outBuffer;
        private int bytesWritten;

        public Reciver(String fileName) throws FileNotFoundException {
            this.fileOutput = new FileOutputStream(fileName);
            this.outBuffer = new LinkedList<>();
            this.bytesWritten = 0;
            bufferCleaner();
        }

        private boolean receivePacket() throws IOException {
            Packet packet = factory.getPacket();
            while (packet.flag != FIN) {
                socket.receive(packet.packet);
                packet.read();
                if (packet.connectionId != connectionId) {
                    packet.flag = BAD_PACKET;
                }
                switch (packet.flag) {
                    case DATA:
                        writerAndFlush(packet.checkSeq(packet.seq), packet.data);
                        packet.sendPacket(connectionId, NULL, (short) bytesWritten, DATA);
                        break;
                    case RST:
                        return false;

                }
            }
            int i = 0;
            while (i < 8) {
                packet.sendPacket(connectionId, NULL, (short) bytesWritten, FIN);
                i++;
            }
            return true;
        }

        private void writerAndFlush(int seq, byte[] packet) throws IOException {
            int id = (seq - bytesWritten) / 255;
            if (id > 0 && outBuffer.get(id) == null) {
                outBuffer.set(id, packet);
            }

            Iterator<byte[]> iterator = outBuffer.iterator();
            byte[] packetOnWindow;
            while (iterator.hasNext()) {
                packetOnWindow = iterator.next();
                if (packetOnWindow != null) {
                    fileOutput.write(packetOnWindow);
                    bytesWritten += packetOnWindow.length;
                    iterator.remove();
                } else {
                    break;
                }
            }
            bufferCleaner();
        }

        private void bufferCleaner() {
            while (8 > outBuffer.size()) {
                outBuffer.add(null);
            }
        }

    }

    private class Sender {

        private final FileInputStream fileInput;
        private final LinkedList<byte[]> InputBuffer;
        private final byte[] readBuffer;
        private boolean finished;
        private int seqConfirmed;
        private int unconfirmedSentCount;

        public Sender(String filename) throws IOException {
            this.seqConfirmed = 0;
            this.unconfirmedSentCount = 0;
            this.fileInput = new FileInputStream(filename);
            this.InputBuffer = new LinkedList<>();
            this.readBuffer = new byte[255];
            this.finished = false;
            bufferCleaner();
        }

        private boolean sendData() throws IOException {
            long timeout;
            Packet packet = factory.getPacket();

            while (!finished || !(packet.flag == FIN)) {
                timeout = System.currentTimeMillis();
                while (System.currentTimeMillis() - timeout < TIMEOUT) {
                    socket.receive(packet.packet);
                    packet.read();
                    if (packet.connectionId != connectionId) {
                        packet.flag = BAD_PACKET;
                    }
                    switch (packet.flag) {
                        case DATA:
                            break;
                        case RST:
                            return false;
                    }
                }
            }
            return true;
        }
private boolean sendWindow(){}
        // nastavi buffer na maximalni velikost
        private void bufferCleaner() {
            int bytes;
            // nacte data z baffru
            while (InputBuffer.size() < 8) {
                try {
                    bytes = fileInput.read(readBuffer, 0, readBuffer.length);
                    if (bytes > 0) {
                        InputBuffer.add(Arrays.copyOf(readBuffer, bytes));
                    } else {
                        break;
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Headquarters.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }

    class Factory {

        public Reciver getReciver(String fileName) {
            try {
                return new Reciver(fileName);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Headquarters.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }

        public Packet getPacket() {
            return new Packet();
        }

        public ConnectorHandler getConnector() {
            return new ConnectorHandler();
        }

        public DatagramPacket getDatagramPacket() {
            return new DatagramPacket(new byte[264], 264);
        }

        public byte[] getBytePacket(byte[] data) {
            return new byte[9 + ((data != null) ? data.length : 0)];
        }
    }
}
