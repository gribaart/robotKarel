package robot;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import static robot.Server.existence;

/**
 * Start program class
 *
 * @author Artem Gribanov <gribaart@fel.cvut.cz>
 */
public class RobotUDP {

    private static Logger log = Logger.getLogger(Server.class.getName());

    public static void main(String[] args) {
//        if (args.length == 0) {
        if (false) {
            log.info("Server: java robot.Robot can consider a <port>");
        } else {
            log.info("starting server...\n");
//            int i = Integer.parseInt(args[0]);
            Server n = new Server(3400);
        }
    }
}

/**
 * Server interface
 *
 * @author Artem Gribanov <gribaart@fel.cvut.cz>
 */
interface ServerInt {

    void runServer(int port);

    void stopServer();

}
//
///**
// * Request manager interface
// *
// * @author Artem Gribanov <gribaart@fel.cvut.cz>
// */
//interface MarsMessageHandlerInt {
//    
//    
//}

/**
 * Singleton server class. Listening port and create a new connection.
 *
 * @author Artem Gribanov <gribaart@fel.cvut.cz>
 */
class Server implements ServerInt {

    private static Logger log = Logger.getLogger(Server.class.getName());
    Server sample = null;
    private static int port;
    public static boolean existence = false;
    ServerSocket server = null;
    Socket client = null;

    public Server() {
    }

    public Server(int port) {
        getNewServerSample();
        if (sample != null) {
            sample.runServer(port);
        }

    }

    public synchronized void getNewServerSample() {
        if (!existence) {
            sample = new Server();
        }
    }

    @Override
    public void runServer(int port) {
        if (!(port >= 3000 && port <= 3999)) {
            log.severe("Range of port must be 3000-3999");
        } else {

            try {
//                InetAddress addr = InetAddress.getByName("213.220.197.126");
//                System.out.println(addr);
                server = new ServerSocket(3400);
                System.err.println(server.getInetAddress());
            } catch (IOException e) {
                log.log(Level.SEVERE, "Couldn''t listen to port: {}", port);
                System.exit(-1);
            }

            while (true) {
                client = null;
                try {
                    log.log(Level.INFO, "Waiting for client on port {0}...", server.getLocalPort());
                    client = server.accept();
                    log.info("Just connected to " + client.getRemoteSocketAddress());;
                } catch (IOException e) {
                    log.severe("Fail of client acception");
                }
                MarsMessageHandler c = new MarsMessageHandler(client);
                new Thread(c).start();
//                c.interrupt();

            }
        }
    }

    @Override
    public void stopServer() {
        try {
            client.close();
            server.close();
            log.info("Server stopped");
            System.exit(666);
        } catch (IOException e) {
            log.severe("Fail of closing sockets");
        }
    }
}

/**
 * Singleton server class. Listening port and create a new connection.
 *
 * @author Artem Gribanov <gribaart@fel.cvut.cz>
 */
class MarsMessageHandler extends Thread {

    private static Logger log = Logger.getLogger(MarsMessageHandler.class.getName());
    private boolean work = true;
    private boolean connect = true;
    private PrintWriter out;
    private DataInputStream in;
    private final Socket client;

    public MarsMessageHandler(Socket client) {
        log.info("Make new Mars message hendler on constructor");
        this.client = client;
    }

    @Override
    public void run() {

        Timer timer = new Timer();
        EndConnection task = new EndConnection();
//        timer.schedule(task, 45000);
        log.info("Run timeout");
        try {
            out = new PrintWriter(client.getOutputStream(), true);
            in = new DataInputStream(new BufferedInputStream(client.getInputStream()));
            log.info("in/out ready");
            verificationClient();
        } catch (IOException ex) {
            log.info("Socket closed");
        }

    }

    private void sendMessage(String msg) {

        String str = "";
        switch (msg) {
            case "LOGIN":
                str = "200 LOGIN";
                break;
            case "PASSWORD":
                str = "201 PASSWORD";
                break;
            case "OK":
                str = "202 OK";
                break;
            case "CHECKSUM":
                str = "300 BAD CHECKSUM";
                break;
            case "LOGIN FAILD":
                str = "500 LOGIN FAILED";
                work = false;
                break;
            case "SYNTAX ERROR":
                str = "501 SYNTAX ERROR";
                work = false;
                break;
            case "TIMEOUT":
                str = "502 TIMEOUT";
                log.info("Vremja vyslo!!");
                work = false;
                break;
        }
        log.info("Message send");
        log.info(str);
        out.write(str);
        out.write("\r\n");

        if (!work && connect) {
            connect = false;
            out.flush();
            endConnect();
        } else {
            out.flush();
        }

    }

    private void verificationClient() throws IOException {
        log.info("Start verefication process");
        sendMessage("LOGIN");
        int checksum = readLogin();
        boolean ok = true;

        sendMessage("PASSWORD");
        log.info("Give pass");

        String str = readLine();
        log.info("Check sum and pass");

        if (checksum == 0) {
            sendMessage("LOGIN FAILD");
        } else {
            try {
                if (checksum == Integer.valueOf(str)) {
                    log.info("Verefication is Ok");

                    sendMessage("OK");

                } else {
                    log.info("Lofin faild2");
                    sendMessage("LOGIN FAILD");
                }
            } catch (Exception e) {
                log.info("Hi bro");
                ok = false;
                sendMessage("LOGIN FAILD");
            }
            if (ok) {
                readHead();
            }
        }
    }

    private String readLine() throws IOException {
        char ch, lastChar = ' ';
        boolean tr = true;
        StringBuilder str = new StringBuilder();

        log.info("Start readLine");
        while (work && tr) {
            try {
                ch = (char) in.read();
                log.info(String.valueOf(ch));
                if (lastChar == '\r' && ch == '\n') {
                    str.deleteCharAt(str.length() - 1);
                    tr = false;
                    log.info("While2 stop");
                } else {
                    lastChar = ch;
                    str.append(ch);
                }
            } catch (Exception e) {
                log.info("Big error");
            }

        }

        log.fine("Stop read line");

        return str.toString();
    }

    private int readLogin() throws IOException {
        log.info("Start read of Login");
        int checksum = 0, i = 0;
        char ch = ' ', lastChar = ' ';
        boolean tr = true;

        char[] robot = {'R', 'o', 'b', 'o', 't'};

        for (char c : robot) {
            ch = (char) in.read();
            log.info(String.valueOf(ch));
            if (c != ch) {
                sendMessage("SYNTAX ERROR");
            }
        }

        while (work && tr) {
            log.info(String.valueOf(ch));
            ch = (char) in.read();
            if (lastChar == '\r' && ch == '\n') {
                checksum -= (int) '\r';
                log.info("Stop while");
                tr = false;
            } else {
                lastChar = ch;

                checksum += (int) ch;
                log.info(String.valueOf(checksum));
            }

        }
        log.info(String.valueOf(checksum + 518));
        if (checksum != 0) {
            checksum += 518;
        }
        return checksum;

    }

    private void readHead() throws IOException {
        char ch;
        boolean correct = true;
        String emp;
        while (work && correct) {
            log.info("Start read Head");
            ch = (char) in.read();
//            emp = String.valueOf(ch);
//            if (ch != 'I' && ch != 'F' && ch != '\0') {
            if (ch == '\0') {
                log.info("Tuta stopit");
//                sendMessage("SYNTAX ERROR");
            }
            switch (ch) {
                case 'I':
                    char[] info = {'N', 'F', 'O', ' '};
                    for (char c : info) {
                        ch = (char) in.read();
                        if (ch != c) {
                            correct = false;
                            sendMessage("SYNTAX ERROR");

                        }
                    }
                    if (correct) {
                        log.info("Info header correct");
                        String st = readLine();
                        sendMessage("OK");
                    }
                    break;
                case 'F':
                    char[] foto = {'O', 'T', 'O', ' '};
                    for (char c : foto) {
                        ch = (char) in.read();
                        if (ch != c) {
                            correct = false;
                            sendMessage("SYNTAX ERROR");

                        }
                    }
                    if (correct) {
                        log.info("Foto header correct");
                        readFoto();
//                        sendMessage("OK");

                    }
                    break;
            }
        }

    }

    private void readFoto() throws IOException {
        log.info("Start read Foto");
//        while (work) {
        long i = 0, k = 0, j = 0;
        String name = "";
        if (work) {
            i = size();
        }
        if (work) {
            ArrayList<Object> list = fotoData(i);
            j = (long) list.get(0);
            name = list.get(1).toString();
        }
        if (work) {
            k = checkFoto();
        }
        System.err.println(j);
        System.err.println(k);
        if (i == 0 || j == 0 || k == 0) {
            sendMessage("CHECKSUM");
        } else if (work && j == k && j != 0) {
            log.info("Foto ok!");
            if(name != ""){
                f
            }
            sendMessage("OK");
        } else {

            sendMessage("CHECKSUM");
        }

//        }
    }

    private long checkFoto() throws IOException {
        log.info("Start check foto");
        boolean tr = true;
        long s = 0;
        char ch, lastChar = ' ';
        StringBuilder str = new StringBuilder();

        char[] checksum = {'0', '0', '\\', 'x', '0', '0', '\\', 'x', '0', '0', '\\', 'x', '0', '0'};
        outerloop:
        for (char c : checksum) {

            ch = (char) in.read();
            System.err.println(ch);
            if (lastChar == '\r' && ch == '\n') {
                str.deleteCharAt(str.length() - 1);
                break outerloop;
            } else {
                if ('\\' != ch && 'x' != ch) {
                    if (ch == ' ') {
                        sendMessage("SYNTAX ERROR");
                    }
                    str.append(ch);
                    lastChar = ch;
                }
            }
        }
        String st = str.toString();
        log.info(st);
        try {
            s = Long.parseLong(st, 16);

        } catch (Exception e) {
            sendMessage("SYNTAX ERROR");
        }
        System.out.println(s);

        return s;

    }

    private ArrayList<Object> fotoData(long s) throws IOException {
        log.info("Start read foto data");
        StringBuilder str = new StringBuilder();
        long checksum = 0;
        long i = 0;

        char ch = ' ', lastChar = ' ';

        while (work) {
            ch = (char) in.read();
            System.err.println(ch);
            if ((lastChar == '\r' && ch == '\n') || ch == ' ') {
                sendMessage("SYNATX ERROR");
            } else if (ch == 'x' && lastChar == '\\') {
                checksum -= (long) lastChar;
                i = i - 1;
                break;
            } else {

                lastChar = ch;
                checksum += (long) ch;
                str.append(ch);
                i++;
            }

        }
        String st = str.toString();
        int fotonumber = fotoSave(st);
        log.info("Foto save");
        if (i != s) {
            log.info("Delete foto, nekompletni prinos");
            sendMessage("SYNTAX ERROR");
            st = "";
        }else if(fotonumber == 1000){
            log.info("Foto neni ulozeno - ok");
            
        }
        ArrayList<Object> mass = new ArrayList<>();
        mass.add(checksum);
        mass.add(st);
        
        return mass;
    }

    private int fotoSave(String foto) throws FileNotFoundException {
        int i = 0;
        boolean tru = true;
        String name = "";
        while (work && tru) {
            StringBuilder fileName = new StringBuilder();
            String str = "";
            str += i;
            fileName.append("foto");
            fileName.append(str);
            fileName.append(".png");
            try (PrintWriter out = new PrintWriter(fileName.toString())) {
                out.println(foto);
                out.close();
                name = fileName.toString();
                tru = false;
            } catch (Exception e) {
                if (i == 999) {
                    tru = false;
                    log.info("Neni foto ulozeno, uz jsou vse 999 obsazeno");
                }
                i++;
            }

        }
        return i;
    }

    private long size() throws IOException {
        log.info("Try read size foto");
        boolean s = true;
        char ch, lastChar = ' ';
        StringBuilder str = new StringBuilder();
        long k = 0;

        while (work && s) {
            ch = (char) in.read();
            if (ch != ' ') {
                str.append(ch);
                lastChar = ch;
            } else if ((lastChar == '\r' && ch == '\n') || (ch == '\r' && lastChar == '\r')) {
                s = false;
                sendMessage("SYNTAX ERROR");

            } else {
                s = false;
            }
        }
        try {
            String st = str.toString();
            k = Long.parseLong(st);
            if (k < 1) {
                if (k < 0) {
                    log.info("Delka foto je zaporna!");
                }
                sendMessage("SYNTAX ERROR");
            }
        } catch (Exception e) {
            sendMessage("SYNTAX ERROR");
        }
        return k;
    }

    private void endConnect() {

        if (!client.isClosed()) {

            log.info("Try to stop connection");

            try {
                in.close();
                out.close();
                client.close();
                log.log(Level.INFO, "{0} - client disconnected", this);

            } catch (IOException ex) {
                log.severe("close in/out/client error");
            }
        }
    }
    // help subcluss

    class EndConnection extends TimerTask {

        @Override
        public void run() {
            if (work) {
                sendMessage("TIMEOUT");
            }
        }
    }
}
